from minibank.infrastructure.sqlalchemy_event_repository.event_store import SQLAlchemyEventStore
from minibank.infrastructure.entity_repository.in_memory import InMemoryEntityRepository
from minibank.infrastructure.email_service import email_service

from minibank.adapters.outgoing.sql_event_store_adapter import SQLEventStoreAdapter
from minibank.adapters.outgoing.email_service_adapter import EmailServiceAdapter

from minibank.domain.services.account import AccountService
from minibank.domain.services.client import ClientService

from minibank.queries.client import ClientQuery
from minibank.queries.account import AccountQuery

from minibank.adapters.incoming.rest_api_adapter import RestApiAdapter

# Infrastructure
event_store = SQLAlchemyEventStore()
event_store.setup_tables()
entity_repository = InMemoryEntityRepository(event_store)

# Domain
client_service = ClientService()
account_service = AccountService()

# Adapters
sql_adapter = SQLEventStoreAdapter(event_store)
rest_api_adapter = RestApiAdapter(client_service, account_service, entity_repository)

email_service_adapter = EmailServiceAdapter(email_service)

# Queries
client_query = ClientQuery(entity_repository)
account_query = AccountQuery(entity_repository)
