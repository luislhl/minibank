class ClientQuery(object):
    def __init__(self, entity_repository):
        self._entity_repository = entity_repository

    def get_accounts(self, client_id):
        accounts = [
            {'id': str(a.id), 'balance': a.balance} for a in self._entity_repository.get_client_accounts(client_id)
        ]

        print()
        print('[DEBUG] Accounts of client {}:'.format(client_id))
        print(accounts)

        return accounts
