class AccountQuery(object):
    def __init__(self, entity_repository):
        self._entity_repository = entity_repository

    def get_transactions(self, account_id):
        transactions = []
        balance = 0

        for event in self._entity_repository.get_account_transactions(account_id):
            amount = event.value - balance
            balance = event.value

            transactions.append({
                'type': type(event).__name__,
                'amount': amount
            })

        print()
        print('[DEBUG] Transactions of account {}:'.format(account_id))
        print(transactions)

        return transactions
