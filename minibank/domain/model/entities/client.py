from minibank.domain.model.events import ClientCreated, AttributeChanged, Discarded, publish

# TODO Base class for entity
class Client(object):
    """
    Client domain entity.
    """
    def __init__(self, entity_id, name, entity_version=0):
        self._id = entity_id
        self._version = entity_version
        self._is_discarded = False
        self._name = name

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, {
            'id': self._id,
            'name': self._name,
            'version': self._version,
            'is_discarded': self._is_discarded
        })

    @property
    def id(self):
        return self._id

    @property
    def version(self):
        return self._version

    @property
    def is_discarded(self):
        return self._is_discarded

    @property
    def name(self):
        return self._name

    def discard(self):
        assert not self._is_discarded

        # Construct a 'Discarded' event object.
        event = Discarded(
            entity_id=self.id,
            entity_version=self.version
        )

        # Apply the event to self.
        apply(self, event)

        # Publish the event for others.
        publish(event)


def apply(entity, event):
    """
    Applies an event to a client instance
    """

    if entity is not None:
        assert isinstance(entity, Client)
        assert entity.id == event.entity_id

    # Handle "created" events by constructing the entity object.
    if isinstance(event, ClientCreated):
        assert entity is None
        entity = Client(event.entity_id, event.name)
        entity._version += 1
        return entity

    # Handle "value changed" events by setting the named value.
    elif isinstance(event, AttributeChanged):
        assert entity is not None
        assert not entity.is_discarded

        setattr(entity, '_' + event.name, event.value)
        entity._version += 1
        return entity

    # Handle "discarded" events by returning 'None'.
    elif isinstance(event, Discarded):
        assert entity is not None
        assert not entity.is_discarded

        entity._version += 1
        entity._is_discarded = True
        return None
    else:
        raise NotImplementedError(type(event))
