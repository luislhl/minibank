from minibank.domain.model.events import AccountCreated, AttributeChanged, Discarded, publish

class Account(object):
    """
    Account domain entity.
    """
    def __init__(self, entity_id, client_id, entity_version=0, balance=0):
        self._id = entity_id
        self._version = entity_version
        self._is_discarded = False
        self._client_id = client_id 
        self._balance = balance

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, {
            'id': self._id,
            'client_id': self._client_id,
            'balance': self._balance,
            'version': self._version,
            'is_discarded': self._is_discarded
        })


    @property
    def id(self):
        return self._id

    @property
    def version(self):
        return self._version

    @property
    def is_discarded(self):
        return self._is_discarded

    @property
    def balance(self):
        return self._balance

    @property
    def client_id(self):
        return self._client_id

    def discard(self):
        assert not self._is_discarded

        # Construct a 'Discarded' event object.
        event = Discarded(
            entity_id=self.id,
            entity_version=self.version
        )

        # Apply the event to self.
        apply(self, event)

        # Publish the event for others.
        publish(event)


def apply(entity, event):
    """
    Applies an event to an account instance
    """
    if entity is not None:
        assert isinstance(entity, Account)
        assert entity.id == event.entity_id

    # Handle "created" events by constructing the entity object.
    if isinstance(event, AccountCreated):
        assert entity is None
        entity = Account(event.entity_id, event.client_id, balance=event.balance)
        entity._version += 1
        return entity

    # Handle "value changed" events by setting the named value.
    elif isinstance(event, AttributeChanged):
        assert entity is not None
        assert not entity.is_discarded
        setattr(entity, '_' + event.name, event.value)
        entity._version += 1
        return entity

    # Handle "discarded" events by returning 'None'.
    elif isinstance(event, Discarded):
        assert entity is not None
        assert not entity.is_discarded
        entity._version += 1
        entity._is_discarded = True
        return None
    else:
        raise NotImplementedError(type(event))
