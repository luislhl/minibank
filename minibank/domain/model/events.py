""" This module contains the publish-subscribe logic for events.
    Also contains the base class for them.

    Based on:
    https://github.com/johnbywater/eventsourcing/blob/develop/eventsourcing/domain/model/events.py
"""

import time
from collections import OrderedDict

class DomainEvent(object):
    """
    Base class for domain events.

    Implements methods to make instances read-only, comparable
    for equality, have recognisable representations, and hashable.
    """

    def __init__(self, **kwargs):
        """
        Initialises event attribute values directly from constructor kwargs.
        """
        self.__dict__.update(kwargs)

    def __setattr__(self, key, value):
        """
        Inhibits event attributes from being updated by assignment.
        """
        raise AttributeError("DomainEvent attributes are read-only")

    def __repr__(self):
        """
        Returns string representing the type and attribute values of the event.
        """
        sorted_items = tuple(sorted(self.__dict__.items()))
        args_strings = ("{0}={1!r}".format(*item) for item in sorted_items)
        args_string = ', '.join(args_strings)
        return "{}({})".format(self.__class__.__qualname__, args_string)


class EventWithEntityID(DomainEvent):
    def __init__(self, entity_id, **kwargs):
        kwargs['entity_id'] = entity_id
        super(EventWithEntityID, self).__init__(**kwargs)

    @property
    def entity_id(self):
        return self.__dict__['entity_id']


# class EventWithTimestamp(DomainEvent):
#     """
#     For events that have a timestamp value.
#     """

#     def __init__(self, timestamp=None, **kwargs):
#         kwargs['timestamp'] = timestamp or time.time()
#         super(EventWithTimestamp, self).__init__(**kwargs)

#     @property
#     def timestamp(self):
#         return self.__dict__['timestamp']

class Created(EventWithEntityID):
    """
    Can be published when an entity is created.
    """

class ClientCreated(Created):
    """
    Can be published when an client entity is created.
    """
    def __init__(self, name, **kwargs):
        kwargs['name'] = name
        super(ClientCreated, self).__init__(**kwargs)

    @property
    def name(self):
        return self.__dict__['name']

class AccountCreated(Created):
    """
    Can be published when an account entity is created.
    """
    def __init__(self, client_id, balance, **kwargs):
        kwargs['client_id'] = client_id
        kwargs['balance'] = balance
        super(AccountCreated, self).__init__(**kwargs)

    @property
    def client_id(self):
        return self.__dict__['client_id']

    @property
    def balance(self):
        return self.__dict__['balance']

class AttributeChanged(EventWithEntityID):
    """
    Can be published when an attribute of an entity is created.
    """
    def __init__(self, name, value, **kwargs):
        kwargs['name'] = name
        kwargs['value'] = value
        super(AttributeChanged, self).__init__(**kwargs)

    @property
    def name(self):
        return self.__dict__['name']

    @property
    def value(self):
        return self.__dict__['value']


class AccountAttributeChanged(AttributeChanged):
    pass

class AccountDeposit(AccountAttributeChanged):
    pass

class AccountWithdraw(AccountAttributeChanged):
    pass


class Discarded(DomainEvent):
    """
    Published when something is discarded.
    """

_event_handlers = OrderedDict()

def subscribe(handler, predicate=None):
    """
    The predicate is an optional function used to decide if the handler
    should be executed when an event is published.

    It receives the event as argument, and should return True if the handler
    is to be executed.
    """

    if predicate not in _event_handlers:
        _event_handlers[predicate] = []
    _event_handlers[predicate].append(handler)


def unsubscribe(handler, predicate=None):
    if predicate in _event_handlers:
        handlers = _event_handlers[predicate]
        if handler in handlers:
            handlers.remove(handler)
            if not handlers:
                _event_handlers.pop(predicate)


def publish(event):
    print('[DEBUG] Event published: {}'.format(event))
    matching_handlers = []
    for predicate, handlers in _event_handlers.items():
        if predicate is None or predicate(event):
            for handler in handlers:
                if handler not in matching_handlers:
                    matching_handlers.append(handler)
    # print('[DEBUG] Matching handlers: {}'.format(matching_handlers))
    for handler in matching_handlers:
        handler(event)
