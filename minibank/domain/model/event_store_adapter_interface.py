from minibank.domain.model.events import subscribe

class EventStoreAdapterInterface(object):
    def __init__(self, event_store):
        self._event_store = event_store

        subscribe(self._handleEvent)

    def _handleEvent(self):
        raise NotImplementedError
