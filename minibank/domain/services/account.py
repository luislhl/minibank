import uuid

from minibank.domain.model.events import AccountCreated, AccountDeposit, AccountWithdraw, publish
from minibank.domain.model.entities.account import apply

class AccountService(object):
    def create_account(self, client_id):
        # Construct an entity ID.
        entity_id = uuid.uuid4()

        # Construct a 'Created' event object.
        event = AccountCreated(
            entity_id=entity_id,
            balance=0,
            client_id=client_id
        )

        # Use the mutator function to construct the entity object.
        entity = apply(None, event)

        # Publish the event for others.
        publish(event=event)

        print('[DEBUG] Account created {}'.format(entity))

        # Return the new entity.
        return entity

    def deposit_account(self, entity, amount):
        event = AccountDeposit(
            entity_id=entity.id,
            name='balance',
            value=entity.balance + amount,
        )

        entity = apply(entity, event)

        publish(event=event)

        print('[DEBUG] Deposited {} in account {}'.format(amount, entity.id))

        return entity

    def withdraw_account(self, entity, amount):
        event = AccountWithdraw(
            entity_id=entity.id,
            name='balance',
            value=entity.balance - amount
        )

        entity = apply(entity, event)

        publish(event=event)

        print('[DEBUG] Withdrawn {} in account {}'.format(amount, entity.id))

        return entity
