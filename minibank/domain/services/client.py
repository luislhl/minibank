import uuid
from minibank.domain.model.events import ClientCreated, publish
from minibank.domain.model.entities.client import apply

class ClientService(object):
    def create_client(self, name):
        # Construct an entity ID.
        entity_id = uuid.uuid4()

        # Construct a 'Created' event object.
        event = ClientCreated(
            entity_id=entity_id,
            name=name
        )

        # Use the mutator function to construct the entity object.
        entity = apply(None, event)

        # Publish the event for others.
        publish(event=event)

        print('[DEBUG] Client created {}'.format(entity))

        # Return the new entity.
        return entity