import json
from flask import request

from minibank.rest_api.app import APP as app

def initialize_routes(rest_api_adapter, client_query, account_query):
    ### Commands
    @app.route('/client/create/', methods=['POST'])
    def client_create():
        payload = request.get_json()

        return rest_api_adapter.client_create(payload['name'])

    @app.route('/client/<uuid:client_id>/accounts/create/', methods=['POST'])
    def account_create(client_id):
        return rest_api_adapter.account_create(client_id)

    @app.route('/account/<uuid:account_id>/deposit/', methods=['POST'])
    def account_deposit(account_id):
        payload = request.get_json()

        return rest_api_adapter.account_deposit(account_id, payload['amount'])

    @app.route('/account/<uuid:account_id>/withdraw/', methods=['POST'])
    def account_withdraw(account_id):
        payload = request.get_json()

        return rest_api_adapter.account_withdraw(account_id, payload['amount'])

    ### Queries
    @app.route('/client/<uuid:client_id>/accounts/', methods=['GET'])
    def client_accounts(client_id):
        accounts = client_query.get_accounts(client_id)

        return json.dumps({ 'accounts': accounts })

    @app.route('/account/<uuid:account_id>/transactions/', methods=['GET'])
    def account_transactions(account_id):
        transactions = account_query.get_transactions(account_id)

        return json.dumps({ 'transactions': transactions})