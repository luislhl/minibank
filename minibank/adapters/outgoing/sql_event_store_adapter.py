from minibank.domain.model.event_store_adapter_interface import EventStoreAdapterInterface


class SQLEventStoreAdapter(EventStoreAdapterInterface):
    def __init__(self, event_store):
        super(SQLEventStoreAdapter, self).__init__(event_store)

    def _handleEvent(self, event):
        self._storeEvent(event)

    def _storeEvent(self, event):
        print('[DEBUG] Will store event {}'.format(event))
        self._event_store.store_event(event)
