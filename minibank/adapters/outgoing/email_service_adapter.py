from minibank.domain.model.events import subscribe, AccountCreated

from minibank.infrastructure.email_service.email_service import send_mail

class EmailServiceAdapter(object):
    def __init__(self, email_service):
        self._email_service = email_service
        subscribe(self._handleEvent)

    def _handleEvent(self, event):
        if isinstance(event, AccountCreated):
            self._sendAccountCreatedMail(event.client_id)

    def _sendAccountCreatedMail(self, client_id):
        msg = "Nova conta criada para o client id {}".format(client_id)

        print('[DEBUG] Will send account created mail')
        send_mail("desafio.stone@gmail.com", msg)
