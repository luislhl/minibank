from minibank.domain.model.event_store_adapter_interface import EventStoreAdapterInterface


class InmemoryEventStoreAdapter(EventStoreAdapterInterface):
    def __init__(self, event_store):
        super(InmemoryEventStoreAdapter, self).__init__(event_store)

    def _handleEvent(self, event):
        self._storeEvent(event)

    def _storeEvent(self, event):
        print('[DEBUG] Will store event in memory {}'.format(event))
        self._event_store.store_event(event)
