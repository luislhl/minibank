import json

class RestApiAdapter(object):
    def __init__(self, client_service, account_service, entity_repository):
        self._client_service = client_service
        self._account_service = account_service
        self._entity_repository = entity_repository 

    def client_create(self, client_name):
        client = self._client_service.create_client(client_name)

        return json.dumps(dict(
            id = str(client.id),
            version = client.version,
            name = client.name,
            is_discarded = client.is_discarded
        )) 

    def account_create(self, client_id):
        account = self._account_service.create_account(client_id)

        return json.dumps(dict(
            id = str(account.id),
            version = account.version,
            client_id = str(account.client_id),
            balance = account.balance,
            is_discarded = account.is_discarded
        )) 

    def account_deposit(self, account_id, amount):
        assert amount > 0

        account = self._entity_repository.get_entity(account_id)

        account = self._account_service.deposit_account(account, amount)

        return json.dumps(dict(
            id = str(account.id),
            version = account.version,
            client_id = str(account.client_id),
            balance = account.balance,
            is_discarded = account.is_discarded
        )) 

    def account_withdraw(self, account_id, amount):
        assert amount > 0

        account = self._entity_repository.get_entity(account_id)

        account = self._account_service.withdraw_account(account, amount)

        return json.dumps(dict(
            id = str(account.id),
            version = account.version,
            client_id = str(account.client_id),
            balance = account.balance,
            is_discarded = account.is_discarded
        )) 
