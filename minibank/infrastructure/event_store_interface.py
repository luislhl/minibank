class EventStoreInterface(object):
    def load_all_events(self):
        raise NotImplementedError()

    def store_event(self):
        raise NotImplementedError()
