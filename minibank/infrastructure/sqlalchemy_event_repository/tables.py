import uuid
import json

from sqlalchemy.sql.schema import Column, Index
from sqlalchemy.sql.sqltypes import BigInteger, String, Text
from sqlalchemy_utils import UUIDType

from minibank.domain.model.events import (
    DomainEvent,
    EventWithEntityID,
    ClientCreated,
    AccountCreated,
    AttributeChanged,
    AccountDeposit,
    AccountWithdraw
)
from .event_store import Base

class EventRecord(Base):
    __tablename__ = 'sequenced_items'

    event_id = Column(UUIDType(), primary_key=True)

    order = Column(BigInteger(), primary_key=True)

    type = Column(String(255))

    data = Column(Text())

    __table_args__ = Index('index', 'event_id', 'order'),

    @classmethod
    def create_from_domain_event(cls, event):
        assert isinstance(event, DomainEvent)

        serializable_event = event.__dict__

        if isinstance(event, EventWithEntityID):
            serializable_event['entity_id'] = str(event.entity_id)
        if isinstance(event, AccountCreated):
            serializable_event['client_id'] = str(event.client_id)

        return cls(
            event_id=uuid.uuid4(),
            type=type(event).__name__,
            order=1, # TODO PK auto increment is not working on SQLite
            data=json.dumps(serializable_event)
        )

    def to_domain_event(self):
        event_data = json.loads(self.data)

        uuid_fields = ['entity_id', 'client_id']

        for field in uuid_fields:
            if field in event_data:
                event_data[field] = uuid.UUID(event_data[field])

        # TODO TODO
        if self.type == ClientCreated.__name__:
            domain_event = ClientCreated(**event_data)
        elif self.type == AccountCreated.__name__:
            domain_event = AccountCreated(**event_data)
        elif self.type == AttributeChanged.__name__:
            domain_event = AttributeChanged(**event_data)
        elif self.type == AccountDeposit.__name__:
            domain_event = AccountDeposit(**event_data)
        elif self.type == AccountWithdraw.__name__:
            domain_event = AccountWithdraw(**event_data)

        return domain_event
