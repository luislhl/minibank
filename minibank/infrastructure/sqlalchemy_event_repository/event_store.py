from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from minibank.infrastructure.event_store_interface import EventStoreInterface

Base = declarative_base()

from .tables import EventRecord

DEFAULT_DB_URI = 'sqlite:///minibank.db'

class SQLAlchemyEventStore(EventStoreInterface):
    def __init__(self, uri=DEFAULT_DB_URI, session=None):
        self._engine = create_engine(uri)
        self._base = Base
        self._session = session

    def setup_tables(self):
        print('Setting up tables...')
        self._base.metadata.create_all(self._engine)

    @property
    def session(self):
        if self._session is None:
            session_maker = sessionmaker(bind=self._engine)
            self._session = session_maker()

        return self._session

    def store_event(self, event):
        event_record = EventRecord.create_from_domain_event(event)
        self.session.add(event_record)
        self.session.commit()

    def load_all_events(self):
        records = self.session.query(EventRecord)

        return [record.to_domain_event() for record in records]
