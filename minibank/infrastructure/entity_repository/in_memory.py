from minibank.infrastructure.event_store_interface import EventStoreInterface
from minibank.domain.model.entities.client import apply as apply_client
from minibank.domain.model.entities.account import apply as apply_account
from minibank.domain.model.events import subscribe, ClientCreated, AccountCreated, AttributeChanged, AccountAttributeChanged

# TODO Make an interface for entity repositories
class InMemoryEntityRepository(object):
    def __init__(self, event_store):
        assert isinstance(event_store, EventStoreInterface)

        self._event_store = event_store
        # Map of all entities by id
        self._entities = {}
        # Map of all accounts by client id
        self._accounts = {}
        # Map of all transactions by account id
        self._transactions = {}

        self._replay_events_from_db()

        subscribe(self._apply_domain_event)

    @property
    def entities(self):
        return self._entities

    @property
    def accounts(self):
        return self._accounts

    @property
    def transactions(self):
        return self._transactions

    def _replay_events_from_db(self):
        print('Replaying events from DB...')

        events = self._event_store.load_all_events()

        for event in events:
            self._apply_domain_event(event)

        print('[DEBUG] Entities after replaying:')
        print(self._entities)

    def _apply_domain_event(self, event):
        if not event.entity_id in self._entities:
            current_entity = None
        else:
            current_entity = self._entities[event.entity_id]

        if isinstance(event, ClientCreated):
            entity = apply_client(current_entity, event)
        elif isinstance(event, AccountCreated):
            entity = apply_account(current_entity, event)
            self._new_account(entity)
        elif isinstance(event, AttributeChanged):
            if isinstance(event, AccountAttributeChanged):
                self._new_transaction(event)
            # TODO TODO TODO Use a general apply function.
            entity = apply_account(current_entity, event)
        else:
            raise NotImplementedError('{} do not know how to handle event {}'.format(type(self).__name__, type(event).__name__))

        print('[DEBUG] Adding new entity to repository:')
        print(entity)
        self._entities[event.entity_id] = entity

    def _new_account(self, account):
        client_id = account.client_id
        assert client_id is not None

        if not client_id in self._accounts:
            self._accounts[client_id] = [account]
        else:
            self._accounts[client_id].append(account)

    def _new_transaction(self, event):
        account_id = event.entity_id
        assert account_id is not None

        if not account_id in self._transactions:
            self._transactions[account_id] = [event]
        else:
            self._transactions[account_id].append(event)

    def get_entity(self, entity_id):
        print('[DEGUB] Entities:')
        print(self._entities)
        if not entity_id in self._entities:
            raise ValueError('Entity not found')

        return self._entities[entity_id]

    def get_client_accounts(self, client_id):
        return self._accounts[client_id]

    def get_account_transactions(self, account_id):
        return self._transactions[account_id]
