from minibank.infrastructure.event_store_interface import EventStoreInterface

class InMemoryEventStore(EventStoreInterface):
    def __init__(self):
        self._events = []

    @property
    def events(self):
        return self._events

    def store_event(self, event):
        self._events.append(event)

    def load_all_events(self):
        return self._events
