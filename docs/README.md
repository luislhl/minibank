# Minibank Documentation

## Overview

This project aims to solve the Minibank Code Challenge, which consists of implementing the backend of a simple bank, offering the following functionalities:

* A person or company can create one or more Bank Accounts;

* On each account, the client can deposit or withdraw money;

* Whenever an account is created your CFO needs to receive an email;

* The history of an account's transactions is available to the client as well as a summary of the client's accounts.

To achieve this, I tried to follow the challenge guidelines, combining a DDD (Domain Driven Design) approach with Event Sourcing, and using a REST API as the service interface.

## Structure

There are 5 main layers in the project: Domain, Adapters, Infrastructure, Rest API and Queries.

### Domain

Contains the domain code, which was kept logically isolated from the rest of the code by using the `Hexagonal Architecture` pattern.

The domain includes its entities (Client and Account), events and services.

To provide isolation between the domain and the Outgoing (Secondary) Adapters, a small pub-sub logic was also implemented in the domain. It is used by Outgoing Adapters to listen and react to events, maintaining the domain unaware of the Adapters.

### Adapters

Includes the incoming and outgoing adapters.

Incoming are the ones who call the domain to perform some operation on it.

Outgoing are called by the domain (through the pub-sub logic, i.e. they just listen to events emitted by the domain).

#### Rest API Adapter

Is the incoming adapter to the Rest API. It's called by it by the endpoints that update the domain entities (Client/Account creation, and Account deposits/withdrawals)

#### Email Service Adapter

An outgoing adapter that listens to `AccountCreated` events, to send an email whenever this event is fired.

#### SQL Event Store Adapter

Listens to events and calls the SQL Event Store in the infrastructure layer, in order to persist events on the DB.

#### In-memory Event Store Adapter

Same as `SQL Event Store Adapter`, but just keeps events in memory. (For tests only)

### Rest API

Uses the Flask web-server to easily provide routing capabilities.
The API includes 6 endpoints:

* `/client/create/ - POST`
* `/client/<uuid:client_id>/accounts/create/ - POST`
* `/account/<uuid:account_id>/deposit/ - POST`
* `/account/<uuid:account_id>/withdraw/ - POST`
* `/client/<uuid:client_id>/accounts/ - GET`
* `/account/<uuid:account_id>/transactions/ - GET`

For POST endpoints, it calls the Rest API Adapter to perform operations on the domain.

For GET endpoints, the modules in the `Queries` layer are called to return the requested info.

### Infrastructure

#### Event Stores
The Event Stores (both SQL and In-memory) are located in this layer, which are called by their respective Outgoing Adapter to persist events.

The SQL Event Store uses the `SQL Alchemy` ORM. It allows us to easily migrate from one SQL DBMS to another, without touching the code. Currently it's setup to use `SQLite`, for simplicity, but in a production deploy we could easily change it to use another option supported by it.

#### Email Service
Also the Email Service, which is called by the Email Adapter to send emails.

#### Entity Repository
Finally, the Entity Repository, which is as in-memory store for all entities that exist on the system. It also maintains the list of accounts for a given client, and the list of transactions for a given account, to facilitate the recovery of these info by Queries.

To do this, the Entity Repository also listens to all events emitted by the pub-sub module of the Domain, reacting accordingly. It also rebuilds itself, replaying the events from the DB, when the system starts.

##### Important Note

The Entity Repository currently keeps the objects just in-memory. It was kept this way in this demonstration just for simplicity.
An improvement for this would be to use Redis or something similar, instead of memory, to allow the distribution of the system in many separate instances.

### Queries

The Queries just recover information from the Entity Repository and format them to be returned by the Rest API in GET endpoints. 