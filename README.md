# Minibank

## Prerequisites 

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Python 3.x](https://www.python.org/)
* [Pip](https://pypi.python.org/pypi/pip/)
* [SQLite](https://www.sqlite.org/)
* [Virtualenv](https://virtualenv.pypa.io/en/stable/) (Optional, but highly recommended)

## Installation

### Linux/Mac 

* `git clone <repository-url>` this repository
* Change into the newly created directory
* If you want to isolate the project's dependencies (recommended), create a new virtualenv with:
> virtualenv --python=python3 env
* Activate the virtualenv:
> . env/bin/activate
* Install dependencies: 
> pip install -r requirements.txt

## Running/Development

* Activate the virtualenv (if not yet activated)
* To run the app, just execute the `app.py` file:
> python app.py

### Running Tests

* To run the unit tests:
> python -m pytest tests/unit_tests/

* To run the integration tests:
> python -m pytest tests/integration_tests/

* To run all the tests:
> python -m pytest tests/

## Documentation

You can check the full documentation [here](docs/)

If you just want to try it quickly, use the following example API calls:

### Example API Calls

#### Client Creation

```
curl -X POST \
  http://127.0.0.1:5000/client/create/ \
  -H 'content-type: application/json' \
  -d '{
	"name": "Nome do Cliente"
}'
```

Should return:  

```{"id": "<client_id>", "version": 1, "name": "Nome do Cliente", "is_discarded": false}```

#### Account Creation

```
curl -X POST \
  http://127.0.0.1:5000/client/<client_id>/accounts/create/
```

Should return:  

```{"id": "<account_id>", "version": 1, "client_id": "<client_id>", "balance": 0, "is_discarded": false}```

#### Account Deposit

```
curl -X POST \
  http://127.0.0.1:5000/account/<account_id>/deposit/ \
  -H 'content-type: application/json' \
  -d '{
	"amount": 7.5
}'
```

Should return:  

```{"id": "<account_id>", "version": 3, "client_id": "<client_id>", "balance": 7.5, "is_discarded": false}```

#### Account Withdraw

```
curl -X POST \
  http://127.0.0.1:5000/account/<account_id>/withdraw/ \
  -H 'content-type: application/json' \
  -d '{
	"amount": 5
}'
```

Should return:  

```{"id": "<account_id>", "version": 5, "client_id": "<client_id>", "balance": 2.5, "is_discarded": false}```

#### List Client's Accounts 
```
curl -X GET \
  http://127.0.0.1:5000/account/<account_id>/transactions/ \
```

Should return:  

```{"transactions": [{"type": "AccountDeposit", "amount": 7.5}, {"type": "AccountWithdraw", "amount": -5.0}]}```

#### List Client's Accounts 
```
curl -X GET \
  http://127.0.0.1:5000/client/<client_id>/accounts/ \
```

Should return:  

```{"accounts": [{"id": "<account_id>", "balance": 2.5}]}```

## Author

* Luis Helder (dev.luislhl@gmail.com)
