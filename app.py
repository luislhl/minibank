from minibank.minibank import rest_api_adapter, client_query, account_query
from minibank.rest_api.app import APP as rest_app

from minibank.rest_api.routes import initialize_routes

if __name__ == '__main__':
    print('Initializing Rest App...')
    initialize_routes(rest_api_adapter, client_query, account_query)
    rest_app.run()
