import pytest

from minibank.domain.model.entities import client

class TestClientClass(object):
    def test_instance_initialization(self):
        instance = client.Client(1, 'name', 2)

        assert instance.id == 1
        assert instance.version == 2
        assert instance.is_discarded is False
        assert instance.name == 'name'

    def test_instance_initialization_default_params(self):
        instance = client.Client(1, 'name')

        assert instance.id == 1
        assert instance.version == 0
        assert instance.is_discarded is False
        assert instance.name == 'name'

    def test_discard_method(self, mocker):
        mocker.patch.object(client, 'apply')
        mocker.patch.object(client, 'publish')
        mock_discarded = mocker.patch.object(client, 'Discarded')

        instance = client.Client(1, 'name', 2)
        instance.discard()

        mock_discarded.assert_called_once_with(entity_id=1, entity_version=2)
        client.apply.assert_called_once_with(instance, mock_discarded())
        client.publish.assert_called_once_with(mock_discarded())

class TestApplyFunction(object):
    @pytest.mark.parametrize('test_type, arg', [
        ('passing an object', object()),
        ('passing a number', 1),
        ('passing a string', 'entity')
    ])
    def test_call_passing_something_not_an_instance_of_client_entity(self, mocker, test_type, arg):
        print(test_type)
        mock_event = mocker.MagicMock(spec=client.ClientCreated)

        with pytest.raises(AssertionError):
            client.apply(arg, mock_event)

    @pytest.mark.parametrize('test_type, event_class', [
        ('event AttributeChanged', client.AttributeChanged),
        ('event Discarded', client.Discarded)
    ])
    def test_call_passing_entity_and_event_with_different_ids(self, mocker, test_type, event_class):
        print(test_type)
        mock_event = mocker.MagicMock(spec=event_class)
        mock_event.entity_id = 1

        with pytest.raises(AssertionError):
            client.apply(client.Client(2, 'name'), mock_event)

    def test_call_passing_client_created_event(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_client_created = mocker.MagicMock(spec=client.ClientCreated)
        mock_client_created.entity_id = '1234'
        mock_client_created.name = 'amazing client'

        entity = client.apply(None, mock_client_created)

        assert entity.id == '1234'
        assert entity.name == 'amazing client'
        assert entity.version == 1
 
    def test_call_passing_client_created_event_and_an_entity(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_client_created = mocker.MagicMock(spec=client.ClientCreated)

        with pytest.raises(AssertionError):
            entity = client.apply(client.Client(1, 'name'), mock_client_created)

    def test_call_passing_attribute_changed_event(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_attribute_changed = mocker.MagicMock(spec=client.AttributeChanged)
        mock_attribute_changed.entity_id = '1234'
        mock_attribute_changed.name = 'name'
        mock_attribute_changed.value = 'amazing client'

        instance = client.Client('1234', 'name', 2)

        entity = client.apply(instance, mock_attribute_changed)

        assert entity.name == 'amazing client'
        assert entity.version == 3

    def test_call_passing_attribute_changed_event_and_entity_none(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_attribute_changed = mocker.MagicMock(spec=client.AttributeChanged)

        with pytest.raises(AssertionError):
            entity = client.apply(None, mock_attribute_changed)

    def test_call_passing_attribute_changed_event_and_a_discarded_entity(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_attribute_changed = mocker.MagicMock(spec=client.AttributeChanged)
        mock_attribute_changed.entity_id = '1234'

        instance = client.Client('1234', 'name', 2)
        instance._is_discarded = True

        with pytest.raises(AssertionError):
            entity = client.apply(instance, mock_attribute_changed)

    def test_call_passing_discarded_event(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_discarded = mocker.MagicMock(spec=client.Discarded)
        mock_discarded.entity_id = '1234'

        instance = client.Client('1234', 'name', 2)

        entity = client.apply(instance, mock_discarded)

        assert entity is None
        assert instance.is_discarded == True
        assert instance.version == 3

    def test_call_passing_discarded_event_and_entity_none(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_discarded = mocker.MagicMock(spec=client.Discarded)

        with pytest.raises(AssertionError):
            entity = client.apply(None, mock_discarded)

    def test_call_passing_discarded_event_and_a_discarded_entity(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_discarded = mocker.MagicMock(spec=client.Discarded)
        mock_discarded.entity_id = '1234'

        instance = client.Client('1234', 'name', 2)
        instance._is_discarded = True

        with pytest.raises(AssertionError):
            entity = client.apply(instance, mock_discarded)
