import pytest

from minibank.domain.model.entities import account

class TestAccountClass(object):
    def test_instance_initialization(self):
        instance = account.Account(1, 'client-id', 2, 4)

        assert instance.id == 1
        assert instance.version == 2
        assert instance.is_discarded is False
        assert instance.client_id == 'client-id'
        assert instance.balance == 4

    def test_instance_initialization_default_params(self):
        instance = account.Account(1, 'client-id')

        assert instance.id == 1
        assert instance.version == 0
        assert instance.is_discarded is False
        assert instance.client_id == 'client-id'
        assert instance.balance == 0

    def test_discard_method(self, mocker):
        mocker.patch.object(account, 'apply')
        mocker.patch.object(account, 'publish')
        mock_discarded = mocker.patch.object(account, 'Discarded')

        instance = account.Account(1, 'client-id', 2)
        instance.discard()

        mock_discarded.assert_called_once_with(entity_id=1, entity_version=2)
        account.apply.assert_called_once_with(instance, mock_discarded())
        account.publish.assert_called_once_with(mock_discarded())

class TestApplyFunction(object):
    @pytest.mark.parametrize('test_type, arg', [
        ('passing an object', object()),
        ('passing a number', 1),
        ('passing a string', 'entity')
    ])
    def test_call_passing_something_not_an_instance_of_account_entity(self, mocker, test_type, arg):
        print(test_type)
        mock_event = mocker.MagicMock(spec=account.AccountCreated)

        with pytest.raises(AssertionError):
            account.apply(arg, mock_event)

    @pytest.mark.parametrize('test_type, event_class', [
        ('event AttributeChanged', account.AttributeChanged),
        ('event Discarded', account.Discarded)
    ])
    def test_call_passing_entity_and_event_with_different_ids(self, mocker, test_type, event_class):
        print(test_type)
        mock_event = mocker.MagicMock(spec=event_class)
        mock_event.entity_id = 1

        with pytest.raises(AssertionError):
            account.apply(account.Account(2, 'client-id'), mock_event)

    def test_call_passing_account_created_event(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_account_created = mocker.MagicMock(spec=account.AccountCreated)
        mock_account_created.entity_id = '1234'
        mock_account_created.client_id = 'client-id'
        mock_account_created.balance = 10

        entity = account.apply(None, mock_account_created)

        assert entity.id == '1234'
        assert entity.client_id == 'client-id'
        assert entity.version == 1
        assert entity.balance == 10
 
    def test_call_passing_account_created_event_and_an_entity(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_account_created = mocker.MagicMock(spec=account.AccountCreated)

        with pytest.raises(AssertionError):
            entity = account.apply(account.Account(1, 'client-id'), mock_account_created)

    def test_call_passing_attribute_changed_event(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_attribute_changed = mocker.MagicMock(spec=account.AttributeChanged)
        mock_attribute_changed.entity_id = '1234'
        mock_attribute_changed.name = 'balance'
        mock_attribute_changed.value = 5

        instance = account.Account('1234', 'client-id', 2)

        entity = account.apply(instance, mock_attribute_changed)

        assert entity.balance == 5
        assert entity.version == 3

    def test_call_passing_attribute_changed_event_and_entity_none(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_attribute_changed = mocker.MagicMock(spec=account.AttributeChanged)

        with pytest.raises(AssertionError):
            entity = account.apply(None, mock_attribute_changed)

    def test_call_passing_attribute_changed_event_and_a_discarded_entity(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_attribute_changed = mocker.MagicMock(spec=account.AttributeChanged)
        mock_attribute_changed.entity_id = '1234'

        instance = account.Account('1234', 'client-id')
        instance._is_discarded = True

        with pytest.raises(AssertionError):
            entity = account.apply(instance, mock_attribute_changed)

    def test_call_passing_discarded_event(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_discarded = mocker.MagicMock(spec=account.Discarded)
        mock_discarded.entity_id = '1234'

        instance = account.Account('1234', 'clientiid', 2)

        entity = account.apply(instance, mock_discarded)

        assert entity is None
        assert instance.is_discarded == True
        assert instance.version == 3

    def test_call_passing_discarded_event_and_entity_none(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_discarded = mocker.MagicMock(spec=account.Discarded)

        with pytest.raises(AssertionError):
            entity = account.apply(None, mock_discarded)

    def test_call_passing_discarded_event_and_a_discarded_entity(self, mocker):
        # I had to create a MagicMock to make it pass on the isinstance call in the function
        mock_discarded = mocker.MagicMock(spec=account.Discarded)
        mock_discarded.entity_id = '1234'

        instance = account.Account('1234', 'client-id', 2)
        instance._is_discarded = True

        with pytest.raises(AssertionError):
            entity = account.apply(instance, mock_discarded)
