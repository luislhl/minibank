import pytest
from collections import OrderedDict

from minibank.domain.model import events

class TestDomainEvent(object):
    def test_initialization(self):
        # The initialization should put any args passed into __dict__
        domain_event = events.DomainEvent(abc=2, blah=1.3, desafio='stone')

        assert domain_event.__dict__['abc'] == 2
        assert domain_event.__dict__['blah'] == 1.3
        assert domain_event.__dict__['desafio'] == 'stone'

    def test_set_attr(self):
        domain_event = events.DomainEvent(abc=2, blah=1.3, desafio='stone')

        with pytest.raises(AttributeError):
            domain_event.blah = 3

    def test_repr(self):
        domain_event = events.DomainEvent(abc=2, blah=1.3, desafio='stone')

        assert str(domain_event) == "DomainEvent(abc=2, blah=1.3, desafio='stone')"

class TestEventWithEntityID(object):
    def test_initialization(self):
        # The initialization should put any args passed into __dict__
        event_with_id = events.EventWithEntityID(entity_id=1, abc=2)

        assert event_with_id.__dict__['abc'] == 2
        assert event_with_id.__dict__['entity_id'] == 1
        assert event_with_id.entity_id == 1

    def test_initialization_without_entity_id(self):
        with pytest.raises(TypeError):
            event_with_id = events.EventWithEntityID(abc=2)

    def test_inheritance(self):
        assert issubclass(events.EventWithEntityID, events.DomainEvent) == True

class TestCreated(object):
    def test_inheritance(self):
        assert issubclass(events.Created, events.EventWithEntityID) == True

class TestClientCreated(object):
    def test_initialization(self):
        # The initialization should put any args passed into __dict__
        event = events.ClientCreated(entity_id=1, name='client', abc=2)

        assert event.__dict__['abc'] == 2
        assert event.__dict__['name'] == 'client' 
        assert event.__dict__['entity_id'] == 1
        assert event.entity_id == 1
        assert event.name == 'client'

    def test_initialization_without_name(self):
        with pytest.raises(TypeError):
            event = events.ClientCreated(entity_id=1, abc=2)

    def test_inheritance(self):
        assert issubclass(events.ClientCreated, events.Created) == True

class TestAccountCreated(object):
    def test_initialization(self):
        # The initialization should put any args passed into __dict__
        event = events.AccountCreated(entity_id=1, client_id='client', balance=3, abc=2)

        assert event.__dict__['abc'] == 2
        assert event.__dict__['entity_id'] == 1
        assert event.__dict__['client_id'] == 'client' 
        assert event.__dict__['balance'] == 3
        assert event.entity_id == 1
        assert event.client_id == 'client'
        assert event.balance == 3

    def test_initialization_without_required_params(self):
        with pytest.raises(TypeError):
            event = events.AccountCreated(entity_id=1, client_id='client', abc=2)

        with pytest.raises(TypeError):
            event = events.AccountCreated(entity_id=1, balance=3, abc=2)

    def test_inheritance(self):
        assert issubclass(events.AccountCreated, events.Created) == True

class TestAttributeChanged(object):
    def test_initialization(self):
        # The initialization should put any args passed into __dict__
        event = events.AttributeChanged(entity_id=1, name='balance', value=3, abc=2)

        assert event.__dict__['abc'] == 2
        assert event.__dict__['entity_id'] == 1
        assert event.__dict__['name'] == 'balance' 
        assert event.__dict__['value'] == 3
        assert event.entity_id == 1
        assert event.name == 'balance'
        assert event.value == 3

    def test_initialization_without_required_params(self):
        with pytest.raises(TypeError):
            event = events.AttributeChanged(entity_id=1, name='balance', abc=2)

        with pytest.raises(TypeError):
            event = events.AttributeChanged(entity_id=1, value=3, abc=2)

    def test_inheritance(self):
        assert issubclass(events.AttributeChanged, events.EventWithEntityID) == True

class TestAccountAttributeChanged(object):
    def test_inheritance(self):
        assert issubclass(events.AccountAttributeChanged, events.AttributeChanged) == True

class TestAccountDeposit(object):
    def test_inheritance(self):
        assert issubclass(events.AccountDeposit, events.AccountAttributeChanged) == True

class TestAccountWithdraw(object):
    def test_inheritance(self):
        assert issubclass(events.AccountWithdraw, events.AccountAttributeChanged) == True

@pytest.fixture
def event_handlers_mock():
    original = events._event_handlers
    events._event_handlers = OrderedDict()
    yield events._event_handlers
    events._event_handlers = original

class TestSubscribe(object):
    def test_subscribe(self, mocker, event_handlers_mock):
        mock_predicate = mocker.MagicMock()
        mock_handler = mocker.MagicMock()
        mock_handler2 = mocker.MagicMock()

        events.subscribe(mock_handler, mock_predicate)
        events.subscribe(mock_handler2, mock_predicate)

        assert mock_predicate in event_handlers_mock
        assert event_handlers_mock[mock_predicate][0] is mock_handler
        assert event_handlers_mock[mock_predicate][1] is mock_handler2

    def test_unsubscribe(self, mocker, event_handlers_mock):
        mock_predicate = mocker.MagicMock()
        mock_handler = mocker.MagicMock()
        mock_handler2 = mocker.MagicMock()

        event_handlers_mock[mock_predicate] = [mock_handler, mock_handler2]

        events.unsubscribe(mock_handler, mock_predicate)

        assert mock_handler not in event_handlers_mock[mock_predicate]
        assert event_handlers_mock[mock_predicate][0] is mock_handler2

    def test_publish(self, mocker, event_handlers_mock):
        mock_predicate = lambda x: True
        mock_handler = mocker.MagicMock()

        mock_predicate2 = lambda x: False
        mock_handler2 = mocker.MagicMock()

        mock_event = object()

        event_handlers_mock[mock_predicate] = [mock_handler]
        event_handlers_mock[mock_predicate2] = [mock_handler2]

        events.publish(mock_event)

        mock_handler.assert_called_once_with(mock_event)
        mock_handler2.assert_not_called()
