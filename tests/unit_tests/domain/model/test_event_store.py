from minibank.domain.model import event_store_adapter_interface

class TestEventStoreAdapterInterface(object):
    def test_initialization(self, mocker):
        mock_subscribe = mocker.patch.object(event_store_adapter_interface, 'subscribe')
        mock_event_store = object()

        event_store_adapter = event_store_adapter_interface.EventStoreAdapterInterface(mock_event_store)

        assert event_store_adapter._event_store is mock_event_store
        mock_subscribe.assert_called_once_with(event_store_adapter._handleEvent)

