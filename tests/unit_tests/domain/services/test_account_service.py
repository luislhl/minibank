import pytest

from minibank.domain.services import account

@pytest.fixture
def account_service():
    instance = account.AccountService()
    return instance

@pytest.fixture
def mock_account_entity():
    class MockAccount:
        @property
        def id(self):
            return 'mock_account_id'

        @property
        def balance(self):
            return 10

    return MockAccount()

class TestAccountService(object):
    def test_create_account(self, mocker, account_service):
        mocker.patch.object(account.uuid, 'uuid4', return_value='uuid4')
        mock_apply = mocker.patch.object(account, 'apply')
        mock_publish = mocker.patch.object(account, 'publish')
        mock_account_created = mocker.patch.object(account, 'AccountCreated')

        account_service.create_account('1234')

        mock_account_created.assert_called_once_with(balance=0, client_id='1234', entity_id='uuid4')
        mock_apply.assert_called_once_with(None, mock_account_created())
        mock_publish.assert_called_once_with(event=mock_account_created())

    def test_deposit_account(self, mocker, account_service, mock_account_entity):
        mock_apply = mocker.patch.object(account, 'apply')
        mock_publish = mocker.patch.object(account, 'publish')
        mock_account_deposit = mocker.patch.object(account, 'AccountDeposit')

        account_service.deposit_account(mock_account_entity, 5)

        mock_account_deposit.assert_called_once_with(entity_id='mock_account_id', name='balance', value=15)
        mock_apply.assert_called_once_with(mock_account_entity, mock_account_deposit())
        mock_publish.assert_called_once_with(event=mock_account_deposit())

    def test_withdraw_account(self, mocker, account_service, mock_account_entity):
        mock_apply = mocker.patch.object(account, 'apply')
        mock_publish = mocker.patch.object(account, 'publish')
        mock_account_withdraw = mocker.patch.object(account, 'AccountWithdraw')

        account_service.withdraw_account(mock_account_entity, 5)

        mock_account_withdraw.assert_called_once_with(entity_id='mock_account_id', name='balance', value=5)
        mock_apply.assert_called_once_with(mock_account_entity, mock_account_withdraw())
        mock_publish.assert_called_once_with(event=mock_account_withdraw())