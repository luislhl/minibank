import pytest

from minibank.domain.services import client

@pytest.fixture
def client_service():
    instance = client.ClientService()
    return instance

class TestClientService(object):
    def test_create_client(self, mocker, client_service):
        mocker.patch.object(client.uuid, 'uuid4', return_value='uuid4')
        mock_apply = mocker.patch.object(client, 'apply')
        mock_publish = mocker.patch.object(client, 'publish')
        mock_client_created = mocker.patch.object(client, 'ClientCreated')

        client_service.create_client('name')

        mock_client_created.assert_called_once_with(name='name', entity_id='uuid4')
        mock_apply.assert_called_once_with(None, mock_client_created())
        mock_publish.assert_called_once_with(event=mock_client_created())