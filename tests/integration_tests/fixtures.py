import pytest

from minibank.infrastructure.inmemory_event_repository.event_store import InMemoryEventStore
from minibank.infrastructure.entity_repository.in_memory import InMemoryEntityRepository

from minibank.adapters.incoming.rest_api_adapter import RestApiAdapter
from minibank.adapters.outgoing.inmemory_event_store_adapter import InmemoryEventStoreAdapter
from minibank.adapters.outgoing.email_service_adapter import EmailServiceAdapter

from minibank.domain.services.account import AccountService
from minibank.domain.services.client import ClientService

from minibank.queries.client import ClientQuery
from minibank.queries.account import AccountQuery

from minibank.rest_api.routes import initialize_routes

@pytest.fixture(scope='module')
def event_store():
    return InMemoryEventStore()

@pytest.fixture(scope='module')
def entity_repository(event_store):
    return InMemoryEntityRepository(event_store)

@pytest.fixture(scope='module')
def client_service():
    return ClientService()

@pytest.fixture(scope='module')
def account_service():
    return AccountService()

@pytest.fixture(scope='module')
def event_store_adapter(event_store):
    return InmemoryEventStoreAdapter(event_store)

@pytest.fixture(scope='module')
def rest_api_adapter(client_service, account_service, entity_repository):
    return RestApiAdapter(client_service, account_service, entity_repository)

@pytest.fixture(scope='module')
def client_query(entity_repository):
    return ClientQuery(entity_repository)

@pytest.fixture(scope='module')
def account_query(entity_repository):
    return AccountQuery(entity_repository)

@pytest.fixture(scope='module')
def rest_app(rest_api_adapter, client_query, account_query, event_store_adapter):
    from minibank.rest_api.app import APP

    APP.testing = True
    initialize_routes(rest_api_adapter, client_query, account_query)
    # APP.run()
    return APP.test_client()
