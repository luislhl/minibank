import json

from tests.integration_tests.fixtures import *

from minibank.domain.model.events import ClientCreated
from minibank.domain.model.entities.client import Client

class TestCreateClient(object):
    def test_create_client(self, rest_app, entity_repository, event_store):
        rest_app.post(
            '/client/create/',
            data=json.dumps(dict(name='Amazing Client')),
            content_type='application/json')

        # Event Store assertions
        assert len(event_store.events) == 1

        created_event = event_store.events[0]
        assert isinstance(created_event, ClientCreated)
        assert created_event.name == 'Amazing Client'

        # Entity Repository assertions
        entity_id = created_event.entity_id

        assert len(entity_repository.entities) == 1
        assert len(entity_repository.accounts) == 0
        assert len(entity_repository.transactions) == 0

        assert entity_id in entity_repository.entities
        created_entity = entity_repository.entities[entity_id]

        assert isinstance(created_entity, Client)
        assert created_entity.id == entity_id
        assert created_entity.name == 'Amazing Client'
        assert created_entity.version == 1
        assert created_entity.is_discarded is False
